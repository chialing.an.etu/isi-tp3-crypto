## **ISI TP 3 CRYPTOGRAPHY**

#### **SUBMITTED BY Sankari Balasubramaniyan, Chia-Ling Bragagnolo**

* **QUESTION 1**

**Propose a solution for the deciphering such that it can be possible only by the
two factor authentications of the two persons in charge. For practical reason,
the two factors will be:**

- Each responsible person is given a USB, password and key1

- Their password decrypts a new key - key2

- The first 8 bytes of key1 and key2 of manager1 decrypts a new key - keya

- The last 8 bytes of key1 and key2 of manager2 decrypts a new key - keyb

- The last 8 bytes of keya and the first 8 bytes of keyb decrypts the main key

- Main key decrypts text file containing customer details.

This way, unless their password is correct, the managers wont have access to the system and unless both of them are present they wont be able to decrypt the main key necessary to decrypt the customer data file

* **QUESTION 2**

**Propose an implementation of the services 1.i., 1.ii, 1.iii and 1.iv in bash, zsh or
in python. Moreover, since the putting into service supposes that the usb keys
of the persons in charge and potential the disk have been initialized, propose an
initialisation service 1.v. that initialises the two usb keys and the disk.**


The implementation is proposed in crypto.py file, it is achieved using python programming language.

Function init() puts the system into service. Folder USB1 anf USB2 stores a backup of password and key1 of manager 1 and 2. Folder RAMDISK stores all the keys and mainkey as a backup in encrypted form and DISK folder holds the encrypted customer file. Function user_interface() interacts with the user to get their password, USB and key, verifies and encrypts the file.

* **QUESTION 3**

**Propose an evolution of your solution to allow the representative to substitute,
if needed, the persons in charge when (re)booting the service server. The system
can’t rely on the confusion between a person in charge and its representative. At
the contrary, it have to be able to discriminate between them without changing
the services functioning.**

This can be achieved by given a given usb and password to the respresentatives but, the key remains the same. At the same time, their password decrypts the same key2 as of responsibles. Hence everything remains the same except the usb and password varies. 


